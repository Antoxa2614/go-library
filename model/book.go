package model

type Book struct {
	Id       string `db:"id" json:"id"`
	Title    string `db:"title" json:"title"`
	Busy     bool   `db:"busy" json:"busy"`
	AuthorId string `db:"author_id" json:"author_id"`
	TakenBy  string `db:"book_taken" json:"takenBy"`
}

type BookAdd struct {
	Title    string `json:"title"`
	AuthorId string `json:"authorId"`
}
