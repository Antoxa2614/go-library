package main

import "gitlab.com/antoxa2614/Go-library/model"

//go:generate swagger generate spec -o ../public/swagger.json --scan-models

//swagger:route POST /book/add book bookCreateRequest
//Добавление новой книги.
//responses:
//    200: description: successfully
//    400: description: BadRequest

//swagger:parameters bookCreateRequest
type bookCreateRequest struct {
	//required:true
	//in:body
	Book model.BookAdd
}

//swagger:route GET /book/watchAll book bookWatchAllRequest
//Возврат списка книг.
//responses:
//    200: description: successfully
//    400: description: Internal server Error

//swagger:parameters bookWatchAllRequest

//swagger:response bookWatchAllResponse
type bookWatchAllRequest struct {
	//in:body
	Books []model.Book
}
