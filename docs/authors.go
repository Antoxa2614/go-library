package main

import "gitlab.com/antoxa2614/Go-library/model"

// swagger:route POST /author/add author authorCreateRequest
// Добавление нового автора.
// responses:
//	   200: description: successfully
//	   400: description: BadRequest

// swagger:parameters authorCreateRequest
type authorCreateRequest struct {
	//required:true
	//in:formData
	Author string `json:"author"`
}

// swagger:route GET /author/watchAll author authorWatchAllRequest
// Список всех авторов.
// responses:
//	 200: authorWatchAllResponse
//	 500: description: Internal server Error

// swagger:parameters authorWatchAllRequest

// swagger:response authorWatchAllResponse
type authorWatchAllResponse struct {
	//in:body
	Author []model.Author
}
