package handlers

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/antoxa2614/Go-library/model"
	service "gitlab.com/antoxa2614/Go-library/servise"
	"net/http"
)

type Handler struct {
	serv service.Service
}

func NewHandler(serv *service.Service) *Handler {
	return &Handler{
		serv: *serv,
	}
}
func (h *Handler) InitRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(middleware.Logger)

	//Author
	router.Post("/author/add", h.AddAuthor)
	router.Get("/author/watchAll", h.WatchAllAuthor)

	//User
	router.Post("/user/add", h.AddUser)
	router.Get("/user/watchAll", h.WatchAllUser)
	router.Post("/user/rentBook", h.UserTake)
	router.Post("/user/giveBook", h.UserGive)

	//Book
	router.Post("/book/add", h.AddBook)
	router.Get("/book/watchAll", h.WatchAllBook)

	//swagger
	router.Get("/swagger", SwaggerUI)

	router.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	return router
}

func (h *Handler) AddAuthor(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	err := request.ParseForm()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	authorName := request.FormValue("author")
	if authorName == "" {
		http.Error(writer, "Empty author name", http.StatusBadRequest)
		return
	}
	err = h.serv.Author.Create(authorName)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	writer.WriteHeader(http.StatusOK)

}

func (h *Handler) WatchAllAuthor(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	authors, err := h.serv.Author.FindAll()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}
	writer.WriteHeader(http.StatusOK)
	json.NewEncoder(writer).Encode(authors)
}

func (h *Handler) AddBook(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	book := model.BookAdd{}
	err := json.NewDecoder(request.Body).Decode(&book)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	err = h.serv.Book.Create(book)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	writer.WriteHeader(http.StatusOK)
}

func (h *Handler) WatchAllBook(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	books, err := h.serv.Book.FindAll()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}
	writer.WriteHeader(http.StatusOK)
	json.NewEncoder(writer).Encode(books)

}
func (h *Handler) AddUser(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	err := request.ParseForm()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	firstName := request.FormValue("firstname")
	if firstName == "" {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	lastName := request.FormValue("lastname")
	if lastName == "" {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	err = h.serv.User.Create(firstName, lastName)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	writer.WriteHeader(http.StatusOK)
}

func (h *Handler) WatchAllUser(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	users, err := h.serv.User.FindAll()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	err = json.NewEncoder(writer).Encode(users)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	writer.WriteHeader(http.StatusOK)
}

func (h *Handler) UserTake(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	err := request.ParseForm()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	idBook := request.FormValue("idBook")
	if idBook == "" {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	idUser := request.FormValue("idUser")
	if idUser == "" {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	err = h.serv.User.UserTakeBook(idBook, idUser)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	writer.WriteHeader(http.StatusOK)

}

func (h *Handler) UserGive(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	err := request.ParseForm()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	idBook := request.FormValue("idBook")
	if idBook == "" {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	idUser := request.FormValue("idUser")
	if idUser == "" {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	err = h.serv.User.GiveBook(idBook, idUser)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	writer.WriteHeader(http.StatusOK)
}
