package cmd

import (
	"context"
	"fmt"
	"gitlab.com/antoxa2614/Go-library/db"
	"gitlab.com/antoxa2614/Go-library/handlers"
	"gitlab.com/antoxa2614/Go-library/internal/repository/author"
	"gitlab.com/antoxa2614/Go-library/internal/repository/book"
	"gitlab.com/antoxa2614/Go-library/internal/repository/user"
	service "gitlab.com/antoxa2614/Go-library/servise"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func Run() {
	port := ":8080"
	postgreSqlClient := db.NewClient()
	authorRepository := author.NewAuthorRepository(postgreSqlClient)
	err := authorRepository.Generate()
	if err != nil {
		fmt.Errorf("error generate")
	}
	userRepository := user.NewRepositoryUser(postgreSqlClient)
	err = userRepository.Generate()
	if err != nil {
		fmt.Errorf("error generate")
	}
	bookRepositiry := book.NewRepositoryBook(postgreSqlClient)
	err = bookRepositiry.Generate()
	if err != nil {
		fmt.Errorf("error generate")
	}

	serviceLibrary := service.NewService(authorRepository, bookRepositiry, userRepository)
	handlerLibrary := handlers.NewHandler(serviceLibrary)
	router := handlerLibrary.InitRoutes()

	srv := http.Server{Addr: port, Handler: router}
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
}
