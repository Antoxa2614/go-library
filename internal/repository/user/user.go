package user

import (
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/antoxa2614/Go-library/model"
	"gitlab.com/antoxa2614/Go-library/pkg/generator"
	"log"
)

type RepositoryUser interface {
	Create(firstName, lastName string) error
	FindAll() (u []model.User, err error)
	UserTakeBook(idBook, idUser string) error
	GiveBook(idBook, idUser string) error
	Generate() error
	Close()
}

type repository struct {
	db *sqlx.DB
}

func NewRepositoryUser(client *sqlx.DB) RepositoryUser {
	return &repository{db: client}
}

func (r repository) Create(firstName, lastName string) error {
	query := "INSERT INTO public.user(firstName, lastName) VALUES ($1, $2) RETURNING id"
	_, err := r.db.Exec(query, firstName, lastName)
	if err != nil {
		return err
	}
	return nil
}

func (r repository) FindAll() (u []model.User, err error) {
	var users []model.User
	query := "SELECT * FROM public.user"
	rows, err := r.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		user := model.User{}
		err := rows.Scan(&user.Id, &user.FirstName, &user.LastName)
		if err != nil {
			return nil, err
		}
		defer rows.Close()
		query2 := "SELECT * FROM public.book"
		rows2, err := r.db.Query(query2)
		if err != nil {
			return nil, err
		}
		defer rows2.Close()
		for rows2.Next() {
			book := model.Book{}
			err := rows2.Scan(&book.Id, &book.Title, &book.AuthorId, &book.Busy, &book.TakenBy)
			if err != nil {
				return nil, err
			}
			if book.TakenBy == user.Id {
				user.RentedBook = append(user.RentedBook, book)
			}
		}
		users = append(users, user)

	}
	return users, nil
}

func (r repository) UserTakeBook(idBook, idUser string) error {
	var existUser bool
	query := "SELECT EXIST(SELECT id FROM public.user WHERE id=$1"
	err := r.db.Get(&existUser, query, idUser)
	if err != nil {
		return err
	}
	if existUser == false {
		return fmt.Errorf("the user is not found")
	}

	var book model.Book
	queryBusy := "SELECT busy FROM public.book WHERE id=$1"
	err = r.db.QueryRow(queryBusy, idBook).Scan(&book.Busy)
	if err != nil {
		return err
	}
	if book.Busy == true {
		return errors.New("the book is busy yet")
	}
	queryUpdate := "UPDATE public.book SET busy=$1,book_taken=$2 WHERE id=$3"
	_, err = r.db.Exec(queryUpdate, true, idUser, idBook)
	if err != nil {
		return err
	}
	return nil

}

func (r repository) GiveBook(idBook, idUser string) error {
	var existUser bool
	query := "SELECT EXIST(SELECT id FROM public.user WHERE id=$1"
	err := r.db.Get(&existUser, query, idUser)
	if err != nil {
		return err
	}
	if existUser == false {
		return fmt.Errorf("the user is not found")
	}

	var book model.Book
	query1 := "SELECT busy, book_taken FROM public.book WHERE id=$1"
	err = r.db.QueryRow(query1, idBook).Scan(&book.Busy, &book.TakenBy)
	if err != nil {
		return err
	}
	if book.Busy != true && book.TakenBy != idUser {
		return errors.New("the book is not with this user")
	}
	queryUpdate := "UPDATE public.book SET busy=$1,book_taken=$2 WHERE id=$3"
	_, err = r.db.Exec(queryUpdate, false, 0, idBook)
	if err != nil {
		return err
	}
	return nil
}

func (r repository) Generate() error {
	var count int
	query := "SELECT COUNT (*) FROM public.user"
	err := r.db.Get(&count, query)
	if err != nil {
		return err
	}
	if count < 50 {
		log.Println("старт наполнения таблицы пользователей")
		userGen := generator.GenerationUser()
		for _, us := range userGen {
			err := r.Create(us.FirstName, us.LastName)
			if err != nil {
				return err
			}
		}
		log.Println("наполнение таблицы пользователей завершено")
	}
	return nil
}

func (r repository) Close() {
	r.db.Close()
}
