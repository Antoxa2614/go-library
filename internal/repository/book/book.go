package book

import (
	"errors"
	"github.com/jmoiron/sqlx"
	"gitlab.com/antoxa2614/Go-library/model"
	"gitlab.com/antoxa2614/Go-library/pkg/generator"
	"log"
)

type RepositoryBook interface {
	Create(FakeBook model.BookAdd) error
	FindAll() ([]model.Book, error)
	Close()
	Generate() error
}

type repository struct {
	db *sqlx.DB
}

func NewRepositoryBook(db *sqlx.DB) RepositoryBook {
	return &repository{db: db}
}

func (r repository) Create(FakeBook model.BookAdd) error {
	var exists bool
	queryFind := "SELECT EXISTS (SELECT id FROM public.author WHERE id = $1)"
	_ = r.db.Get(&exists, queryFind, FakeBook.AuthorId)
	if exists == false {
		return errors.New("author with id not found")
	}
	queryAdd := "INSERT INTO public.book (title, author_id, book_taken) VALUES ($1, $2, $3) RETURNING id, title, author_id, busy, book_taken"

	_, err := r.db.Exec(queryAdd, FakeBook.Title, FakeBook.AuthorId, 0)
	if err != nil {
		return err
	}
	return nil
}

func (r repository) FindAll() ([]model.Book, error) {
	var book model.Book
	var books []model.Book
	query := "SELECT * FROM public.book"
	row, err := r.db.Query(query)
	defer row.Close()
	if err != nil {
		return nil, err
	}
	for row.Next() {
		err = row.Scan(&book.Id, &book.Title, &book.AuthorId, &book.Busy, &book.TakenBy)
		if err != nil {
			return nil, err
		}
		books = append(books, book)
	}
	return books, nil
}

func (r repository) Close() {
	r.db.Close()
}

func (r repository) Generate() error {
	var count int
	query := "SELECT COUNT(*) FROM public.book"
	err := r.db.Get(&count, query)
	if err != nil {
		return err
	}
	if count < 100 {
		log.Println("старт наполнения таблицы книг")
		bookGen := generator.GenerationBook()
		for j := range bookGen {
			err = r.Create(bookGen[j])
			if err != nil {
				return err
			}
		}
		log.Println(" наполнение таблицы книг завершено")
	}

	return err

}
