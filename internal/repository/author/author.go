package author

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/antoxa2614/Go-library/pkg/generator"
	"log"

	"gitlab.com/antoxa2614/Go-library/model"
)

type RepositoryAuthor interface {
	Create(name string) error
	FindAll() (u []model.Author, err error)
	Generate() error
}

type repository struct {
	db *sqlx.DB
}

func NewAuthorRepository(client *sqlx.DB) RepositoryAuthor {
	return &repository{db: client}
}

func (r *repository) Generate() error {

	var count int
	query := "SELECT COUNT(*) FROM public.author"
	err := r.db.Get(&count, query)
	if err != nil {
		return err
	}

	if count < 10 {
		log.Println("старт наполнения таблицы авторов")
		authorGen := generator.GenerationAuthor()
		for j := range authorGen {
			err = r.Create(authorGen[j].Name)
			if err != nil {
				return err
			}
		}
		log.Println(" наполнение таблицы авторов завершено")

	}

	return nil
}

func (r *repository) Close() {
	r.db.Close()
}

func (r *repository) Create(author string) error {

	dbf := "INSERT INTO public.author (name) VALUES ($1) RETURNING id"
	_, err := r.db.Exec(dbf, author)
	if err != nil {
		return err
	}

	return nil

}

func (r *repository) FindAll() ([]model.Author, error) {
	var author model.Author
	var authors []model.Author
	query := "SELECT id,name FROM public.author"
	row, err := r.db.Query(query)
	if err != nil {
		return nil, err
	}

	for row.Next() {
		err = row.Scan(&author.ID, &author.Name)
		if err != nil {
			return nil, err
		}
		var book model.Book
		queryBook := "SELECT * FROM public.book"
		row1, err := r.db.Query(queryBook)
		if err != nil {
			return nil, err
		}

		for row1.Next() {
			err = row1.Scan(&book.Id, &book.Title, &book.AuthorId, &book.Busy, &book.TakenBy)
			if err != nil {
				return nil, err
			}
			if author.ID == book.AuthorId {
				author.Books = append(author.Books, book)
			}
		}

		authors = append(authors, author)
		author.Books = nil
	}

	return authors, nil
}
