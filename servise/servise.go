package service

import (
	"gitlab.com/antoxa2614/Go-library/internal/repository/author"
	"gitlab.com/antoxa2614/Go-library/internal/repository/book"
	"gitlab.com/antoxa2614/Go-library/internal/repository/user"
	"gitlab.com/antoxa2614/Go-library/model"
)

type serviceAuthor interface {
	Create(name string) error
	FindAll() (u []model.Author, err error)
	Generate() error
}

type serviceUser interface {
	Create(firstName, lastName string) error
	FindAll() (u []model.User, err error)
	UserTakeBook(idBook, idUser string) error
	GiveBook(idBook, idUser string) error
	Generate() error
}

type serviceBook interface {
	Create(FakeBook model.BookAdd) error
	FindAll() ([]model.Book, error)
	Generate() error
}

type Service struct {
	Author serviceAuthor
	Book   serviceBook
	User   serviceUser
}

func NewService(repAuthor author.RepositoryAuthor, repBook book.RepositoryBook, repUser user.RepositoryUser) *Service {
	return &Service{Author: repAuthor, Book: repBook, User: repUser}
}
