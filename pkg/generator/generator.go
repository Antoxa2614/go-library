package generator

import (
	"github.com/brianvoe/gofakeit/v6"
	"gitlab.com/antoxa2614/Go-library/model"
	"strconv"
)

func GenerationAuthor() []model.Author {
	authors := make([]model.Author, 15)
	for i := 0; i < 15; i++ {
		author := model.Author{
			ID:   strconv.Itoa(i),
			Name: gofakeit.BookAuthor(),
		}
		authors = append(authors, author)
	}
	return authors

}
func GenerationBook() []model.BookAdd {
	books := []model.BookAdd{}
	for i := 0; i < 120; i++ {
		book := model.BookAdd{
			Title:    gofakeit.Sentence(2),
			AuthorId: strconv.Itoa(gofakeit.IntRange(1, 10)),
		}
		books = append(books, book)

	}
	return books
}

func GenerationUser() []model.User {
	users := []model.User{}
	for i := 0; i < 50; i++ {
		user := model.User{Id: strconv.Itoa(i),
			FirstName: gofakeit.FirstName(),
			LastName:  gofakeit.LastName(),
		}
		users = append(users, user)
	}
	return users

}
