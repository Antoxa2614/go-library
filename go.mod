module gitlab.com/antoxa2614/Go-library

go 1.20

require (
	github.com/brianvoe/gofakeit/v6 v6.22.0
	github.com/go-chi/chi/v5 v5.0.8
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.5.1
)

require github.com/lib/pq v1.10.9 // indirect
